// Java code to demonstrate
// bridge design pattern

// abstraction in bridge pattern
internal abstract class Vehicle protected constructor(protected var workShop1: Workshop, protected var workShop2: Workshop) {

    abstract fun manufacture()
}

// Refine abstraction 1 in bridge pattern
internal class Car(workShop1: Workshop, workShop2: Workshop) : Vehicle(workShop1, workShop2) {

    override fun manufacture() {
        print("Car ")
        workShop1.work()
        workShop2.work()
    }
}

// Refine abstraction 2 in bridge pattern
internal class Bike(workShop1: Workshop, workShop2: Workshop) : Vehicle(workShop1, workShop2) {

    override fun manufacture() {
        print("Bike ")
        workShop1.work()
        workShop2.work()
    }
}

// Implementor for bridge pattern
internal interface Workshop {
    fun work()
}

// Concrete implementation 1 for bridge pattern
internal class Produce : Workshop {
    override fun work() {
        print("Produced")
    }
}

// Concrete implementation 2 for bridge pattern
internal class Assemble : Workshop {
    override fun work() {
        print(" And")
        println(" Assembled.")
    }
}

// Demonstration of bridge design pattern
internal object BridgePattern {
    @JvmStatic
    fun main(args: Array<String>) {
        val vehicle1 = Car(Produce(), Assemble())
        vehicle1.manufacture()
        val vehicle2 = Bike(Produce(), Assemble())
        vehicle2.manufacture()
    }
}