interface Pizza {
    fun makePizza(): String
}

class BasePizza(private val type: String) : Pizza {
    override fun makePizza(): String {
        return type
    }
}

class MeatTopping(private val pizza: Pizza, private val topping: String) : Pizza {
    override fun makePizza(): String {
        return pizza.makePizza() + " " + topping
    }
}

class VeggleTopping(private val pizza: Pizza, private val topping: String): Pizza{
    override fun makePizza(): String {
        return pizza.makePizza() + " " + topping
    }
}